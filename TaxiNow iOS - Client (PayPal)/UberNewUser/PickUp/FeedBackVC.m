//
//  FeedBackVC.m
//  UberNewUser
//
//  Created by Deep Gami on 01/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "FeedBackVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "PickUpVC.h"
#import "PayPalPayment.h"
#import "PayPalAdvancedPayment.h"
#import "PayPalAmounts.h"
#import "PayPalReceiverAmounts.h"
#import "PayPalAddress.h"
#import "PayPalInvoiceData.h"
#import "PayPalInvoiceItem.h"

#define SPACING 3.

@interface FeedBackVC ()
{
    UIButton *payPalBtn;
}

@end

@implementation FeedBackVC

#pragma mark - ViewLife Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
   // [super setBackBarItem];
    
    
    [self customFont];
   
    
    NSArray *arrName=[self.strFirstName componentsSeparatedByString:@" "];
    
    self.lblFirstName.text=[arrName objectAtIndex:0];
    self.lblLastName.text=[arrName objectAtIndex:1];
    
    self.lblDistance.textColor=[UberStyleGuide colorDefault];
    self.lblTIme.textColor=[UberStyleGuide colorDefault];
    
    self.lblDistance.text=[self.dictWalkInfo valueForKey:@"distance"];
    self.lblTIme.text=[self.dictWalkInfo valueForKey:@"time"];
    [self.imgUser applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    [self.imgUser downloadFromURL:self.strUserImg withPlaceholder:nil];
    self.viewForBill.hidden=NO;
    
    [self customSetup];
    [self setPriceValue];
    
    //    [self addLabelWithText:@"Parallel Payment" andButtonWithType:BUTTON_294x43 withAction:@selector(parallelPayment)];
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSLog(@"Payment Mode - %@",[pref objectForKey:[pref objectForKey:PREF_PAYMENT_METHOD]]);
    
    if ([[pref objectForKey:PREF_PAYMENT_METHOD] isEqualToString:@"2"]) {
        status = PAYMENTSTATUS_CANCELED;
        
        payPalBtn = [[UIButton alloc]init];
        payPalBtn =  [[PayPal getPayPalInst]
                   getPayButtonWithTarget:self
                   andAction:@selector(parallelPayment)
                   andButtonType:BUTTON_294x43
                   andButtonText:BUTTON_TEXT_PAY];
        [payPalBtn setFrame:CGRectMake(13, self.view.frame.size.height-65, 294, 43)];
        [self.btnConfirm setHidden:YES];
        
        [self.view addSubview:payPalBtn];
    }
    else {
        [self.btnConfirm setHidden:NO];
    }
}

#pragma mark-
#pragma mark- Set Invoice Details

-(void)setPriceValue
{
    self.lblBasePrice.text=[NSString stringWithFormat:@"$ %@",[dictBillInfo valueForKey:@"base_price"]];
    self.lblDistCost.text=[NSString stringWithFormat:@"$ %@",[dictBillInfo valueForKey:@"distance_cost"]];
    self.lblTimeCost.text=[NSString stringWithFormat:@"$ %@",[dictBillInfo valueForKey:@"time_cost"]];
    self.lblTotal.text=[NSString stringWithFormat:@"$ %@",[dictBillInfo valueForKey:@"total"]];
    
    float totalDist=[[dictBillInfo valueForKey:@"distance_cost"] floatValue];
    float Dist=[[dictBillInfo valueForKey:@"distance"]floatValue];
    
    if ([[dictBillInfo valueForKey:@"unit"]isEqualToString:@"kms"])
    {
        totalDist=totalDist*0.621317;
        Dist=Dist*0.621371;
    }
    if(Dist!=0)
    {
        self.lblPerDist.text=[NSString stringWithFormat:@"%.2f$ per Miles",(totalDist/Dist)];
    }
    else
    {
        self.lblPerDist.text=@"0$ per Miles";
    }
    
    float totalTime=[[dictBillInfo valueForKey:@"time_cost"] floatValue];
    float Time=[[dictBillInfo valueForKey:@"time"]floatValue];
    if(Time!=0)
    {
        self.lblPerTime.text=[NSString stringWithFormat:@"%.2f$ per Mins",(totalTime/Time)];
    }
    else
    {
        self.lblPerTime.text=@"0$ per Mins";
    }
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController)
    {
        [self.barButton addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:revealViewController.panGestureRecognizer];
        
    }
}

#pragma mark-
#pragma mark- Custom Font

-(void)customFont
{
    self.lblDistance.font=[UberStyleGuide fontRegular];
    self.lblDistCost.font=[UberStyleGuide fontRegular];
    self.lblBasePrice.font=[UberStyleGuide fontRegular];
    self.lblDistance.font=[UberStyleGuide fontRegular];
    self.lblPerDist.font=[UberStyleGuide fontRegular];
    self.lblPerTime.font=[UberStyleGuide fontRegular];
    self.lblTIme.font=[UberStyleGuide fontRegular];
    self.lblTimeCost.font=[UberStyleGuide fontRegular];
    self.lblTotal.font=[UberStyleGuide fontRegular:30.0f];
    self.lblFirstName.font=[UberStyleGuide fontRegular];
    self.lblLastName.font=[UberStyleGuide fontRegular];
    self.btnFeedBack.titleLabel.font=[UberStyleGuide fontRegular];
    
    self.btnConfirm.titleLabel.font=[UberStyleGuide fontRegularBold];
    self.btnSubmit.titleLabel.font=[UberStyleGuide fontRegularBold];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark-
#pragma makr- Btn Click Events

- (IBAction)submitBtnPressed:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REVIEWING", nil)];
        RBRatings rating=[ratingView getcurrentRatings];
        int rate=rating/2.0;
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:strForUserId forKey:PARAM_ID];
        [dictParam setObject:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setObject:strReqId forKey:PARAM_REQUEST_ID];
        [dictParam setObject:[NSString stringWithFormat:@"%d",rate] forKey:PARAM_RATING];
        [dictParam setObject:self.txtComments.text forKey:PARAM_COMMENT];

        
        
//        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_RATE_DRIVER withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSLog(@"SUBMIT RESP -> %@",response);
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"RATING", nil)];
                     [[NSUserDefaults standardUserDefaults] removeObjectForKey:PREF_REQ_ID];
                     
                     [self.navigationController popToRootViewControllerAnimated:YES];
                     
                     /*for (UIViewController *vc in self.navigationController.viewControllers)
                      {
                      if ([vc isKindOfClass:[PickUpVC class]])
                      {
                      [self.navigationController popToViewController:vc animated:YES];
                      return ;
                      }
                      }*/
                 }
             }
             
             [[AppDelegate sharedAppDelegate]hideLoadingView];

         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }

}
#pragma mark -
#pragma mark - UITextField Delegate


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, -150, self.view.frame.size.width, self.view.frame.size.height);
        
        
    } completion:^(BOOL finished)
     {
     }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        
    } completion:^(BOOL finished)
     {
     }];
  
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)confirmBtnPressed:(id)sender
{
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    if ([[pref objectForKey:PREF_PAYMENT_METHOD] isEqualToString:@"2"]) {
        [self parallelPayment];
    }
    else {
        self.viewForBill.hidden=YES;
        ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(120, 20) AndPosition:CGPointMake(135, 152)];
        ratingView.backgroundColor=[UIColor clearColor];
        [self.view addSubview:ratingView];
    }
}

#pragma mark-
#pragma mark- Text Field Delegate

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtComments resignFirstResponder];
}



- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    self.txtComments.text=@"";
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    if ([self.txtComments.text isEqualToString:@""])
    {
        self.txtComments.text=@"Comments";
    }
    
}

#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
- (void)addLabelWithText:(NSString *)text andButtonWithType:(PayPalButtonType)type withAction:(SEL)action {
    UIFont *font = [UIFont boldSystemFontOfSize:14.];
    CGSize size = [text sizeWithFont:font];
    
    //you should call getPayButton to have the library generate a button for you.
    //this button will be disabled if device interrogation fails for any reason.
    //
    //-- required parameters --
    //target is a class which implements the PayPalPaymentDelegate protocol.
    //action is the selector to call when the button is clicked.
    //inButtonType is the button type (desired size).
    //
    //-- optional parameter --
    //inButtonText can be either BUTTON_TEXT_PAY (default, displays "Pay with PayPal"
    //in the button) or BUTTON_TEXT_DONATE (displays "Donate with PayPal" in the
    //button). the inButtonText parameter also affects some of the library behavior
    //and the wording of some messages to the user.
    
    y = 2.;

    UIButton *button = [[PayPal getPayPalInst] getPayButtonWithTarget:self andAction:action andButtonType:type];
    CGRect frame = button.frame;
    frame.origin.x = round((self.view.frame.size.width - button.frame.size.width) / 2.);
    frame.origin.y = round(y + size.height);
    button.frame = frame;
    [self.view addSubview:button];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(frame.origin.x, y, size.width, size.height)];
    label.font = font;
    label.text = text;
    label.backgroundColor = [UIColor clearColor];
    [self.view addSubview:label];
    
    y += size.height + frame.size.height + SPACING;
}
*/

- (void)parallelPayment {
    //dismiss any native keyboards
    //[preapprovalField resignFirstResponder];
    
    //optional, set shippingEnabled to TRUE if you want to display shipping
    //options to the user, default: TRUE
    [PayPal getPayPalInst].shippingEnabled = TRUE;
    
    //optional, set dynamicAmountUpdateEnabled to TRUE if you want to compute
    //shipping and tax based on the user's address choice, default: FALSE
    [PayPal getPayPalInst].dynamicAmountUpdateEnabled = TRUE;
    
    //optional, choose who pays the fee, default: FEEPAYER_EACHRECEIVER
    [PayPal getPayPalInst].feePayer = FEEPAYER_EACHRECEIVER;
    
    //for a payment with multiple recipients, use a PayPalAdvancedPayment object
    PayPalAdvancedPayment *payment = [[PayPalAdvancedPayment alloc] init];
    payment.paymentCurrency = @"USD";
    
    // A payment note applied to all recipients.
    payment.memo = @"A Note applied to all recipients";
    
    //receiverPaymentDetails is a list of PPReceiverPaymentDetails objects
    payment.receiverPaymentDetails = [NSMutableArray array];
    
    //Frank's Robert's Julie's Bear Parts;
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSArray *nameArray = [NSArray arrayWithObjects:@"Admin", @"User", nil];
    NSArray *mailArray = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",[pref objectForKey:PREF_ADMIN_EMAIL]],[NSString stringWithFormat:@"%@",[pref objectForKey:PREF_WALKER_EMAIL]], nil];
    NSArray *amountArray = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",[pref objectForKey:PREF_ADMIN_AMOUNT]],[NSString stringWithFormat:@"%@",[pref objectForKey:PREF_WALKER_AMOUNT]], nil];
//    NSArray *mailArray = [NSArray arrayWithObjects:@"uber@gmail.com",@"uber2@gmail.com", nil];
//    NSArray *amountArray = [NSArray arrayWithObjects:@"10",@"20", nil];
    
    for (int i=0; i<[nameArray count]; i++) {
        PayPalReceiverPaymentDetails *details = [[PayPalReceiverPaymentDetails alloc] init];

//        if (i==0) {
//            details.recipient = @"testpaypal34@gmail.com";
//        }
//        else {
            details.recipient = [NSString stringWithFormat:@"%@", [mailArray objectAtIndex:i]];
//        }
        
        details.merchantName = [NSString stringWithFormat:@"%@",[nameArray objectAtIndex:i]];
//        details.merchantName = @"My Trip Amount";
        details.subTotal = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",[amountArray objectAtIndex:i]]];

        //invoiceData is a PayPalInvoiceData object which contains tax, shipping, and a list of PayPalInvoiceItem objects
        details.invoiceData = [[PayPalInvoiceData alloc] init];
        
        //invoiceItems is a list of PayPalInvoiceItem objects
        //NOTE: sum of totalPrice for all items must equal details.subTotal
        //NOTE: example only shows a single item, but you can have more than one
        details.invoiceData.invoiceItems = [NSMutableArray array];
        PayPalInvoiceItem *item = [[PayPalInvoiceItem alloc] init];
        item.totalPrice = details.subTotal;
        item.name = [nameArray objectAtIndex:i];
        [details.invoiceData.invoiceItems addObject:item];

        [payment.receiverPaymentDetails addObject:details];
    }
    
    /*
    for (int i = 1; i <= 3; i++) {
        PayPalReceiverPaymentDetails *details = [[PayPalReceiverPaymentDetails alloc] init];
        
        // Customize the payment notes for one of the three recipient.
        if (i == 2) {
            details.description = [NSString stringWithFormat:@"Bear Component %d", i];
        }
        
        details.recipient = [NSString stringWithFormat:@"example-merchant-%d@paypal.com", 4 - i];
        details.merchantName = [NSString stringWithFormat:@"%@ Bear Parts",[nameArray objectAtIndex:i-1]];
        
        unsigned long long order, tax, shipping;
        order = i * 100;
        tax = i * 7;
        shipping = i * 14;
        
        //subtotal of all items for this recipient, without tax and shipping
        details.subTotal = [NSDecimalNumber decimalNumberWithMantissa:order exponent:-2 isNegative:FALSE];
        
        //invoiceData is a PayPalInvoiceData object which contains tax, shipping, and a list of PayPalInvoiceItem objects
        details.invoiceData = [[PayPalInvoiceData alloc] init];
        details.invoiceData.totalShipping = [NSDecimalNumber decimalNumberWithMantissa:shipping exponent:-2 isNegative:FALSE];
        details.invoiceData.totalTax = [NSDecimalNumber decimalNumberWithMantissa:tax exponent:-2 isNegative:FALSE];
        
        //invoiceItems is a list of PayPalInvoiceItem objects
        //NOTE: sum of totalPrice for all items must equal details.subTotal
        //NOTE: example only shows a single item, but you can have more than one
        details.invoiceData.invoiceItems = [NSMutableArray array];
        PayPalInvoiceItem *item = [[PayPalInvoiceItem alloc] init];
        item.totalPrice = details.subTotal;
        item.name = @"Bear Stuffing";
        [details.invoiceData.invoiceItems addObject:item];
        
        [payment.receiverPaymentDetails addObject:details];
    }
    */
    
    [[PayPal getPayPalInst] advancedCheckoutWithPayment:payment];
}

-(void)sendPaymentTransactionID:(NSString *)transactionID {
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSString *userID = [pref objectForKey:PREF_USER_ID];
    NSString *userToken = [pref objectForKey:PREF_USER_TOKEN];
    NSString *requestID = [pref objectForKey:PREF_REQ_ID];
    
    [dictParams setValue:userID forKey:PARAM_ID];
    [dictParams setValue:userToken forKey:PARAM_TOKEN];
    [dictParams setValue:requestID forKey:PARAM_REQUEST_ID];
    [dictParams setValue:transactionID forKey:PARAM_PAYPAL_TRANS_ID];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_PAY_BY_PAYPAL withParamData:dictParams withBlock:^(id response, NSError *error) {
        if (response) {
            NSLog(@"Payal API Response - %@",response);
            [[AppDelegate sharedAppDelegate]hideLoadingView];
            
            [payPalBtn removeFromSuperview];
            self.viewForBill.hidden=YES;
            ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(120, 20) AndPosition:CGPointMake(135, 152)];
            ratingView.backgroundColor=[UIColor clearColor];
            [self.view addSubview:ratingView];
        }
        else if (error) {
            [[AppDelegate sharedAppDelegate]hideLoadingView];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Internal Server Error"
                                               message:@"Your Payment has been recieved by PayPal. Once the services has been restored the same will be reflected in Ride History"
                                              delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }];
}

//paymentSuccessWithKey:andStatus: is a required method. in it, you should record that the payment
//was successful and perform any desired bookkeeping. you should not do any user interface updates.
//payKey is a string which uniquely identifies the transaction.
//paymentStatus is an enum value which can be STATUS_COMPLETED, STATUS_CREATED, or STATUS_OTHER
- (void)paymentSuccessWithKey:(NSString *)payKey andStatus:(PayPalPaymentStatus)paymentStatus {
    [self sendPaymentTransactionID:payKey];
    
    NSString *severity = [[PayPal getPayPalInst].responseMessage objectForKey:@"severity"];
    NSLog(@"severity: %@", severity);
    NSString *category = [[PayPal getPayPalInst].responseMessage objectForKey:@"category"];
    NSLog(@"category: %@", category);
    NSString *errorId = [[PayPal getPayPalInst].responseMessage objectForKey:@"errorId"];
    NSLog(@"errorId: %@", errorId);
    NSString *message = [[PayPal getPayPalInst].responseMessage objectForKey:@"message"];
    NSLog(@"message: %@", message);
    
    status = PAYMENTSTATUS_SUCCESS;
}

//paymentFailedWithCorrelationID is a required method. in it, you should
//record that the payment failed and perform any desired bookkeeping. you should not do any user interface updates.
//correlationID is a string which uniquely identifies the failed transaction, should you need to contact PayPal.
//errorCode is generally (but not always) a numerical code associated with the error.
//errorMessage is a human-readable string describing the error that occurred.
- (void)paymentFailedWithCorrelationID:(NSString *)correlationID {
    
    NSString *severity = [[PayPal getPayPalInst].responseMessage objectForKey:@"severity"];
    NSLog(@"severity: %@", severity);
    NSString *category = [[PayPal getPayPalInst].responseMessage objectForKey:@"category"];
    NSLog(@"category: %@", category);
    NSString *errorId = [[PayPal getPayPalInst].responseMessage objectForKey:@"errorId"];
    NSLog(@"errorId: %@", errorId);
    NSString *message = [[PayPal getPayPalInst].responseMessage objectForKey:@"message"];
    NSLog(@"message: %@", message);

    status = PAYMENTSTATUS_FAILED;
}

//paymentCanceled is a required method. in it, you should record that the payment was canceled by
//the user and perform any desired bookkeeping. you should not do any user interface updates.
- (void)paymentCanceled {
    status = PAYMENTSTATUS_CANCELED;
}


//adjustAmountsForAddress:andCurrency:andAmount:andTax:andShipping:andErrorCode: is optional. you only need to
//provide this method if you wish to recompute tax or shipping when the user changes his/her shipping address.
//for this method to be called, you must enable shipping and dynamic amount calculation on the PayPal object.
//the library will try to use the advanced version first, but will use this one if that one is not implemented.
- (PayPalAmounts *)adjustAmountsForAddress:(PayPalAddress const *)inAddress andCurrency:(NSString const *)inCurrency andAmount:(NSDecimalNumber const *)inAmount
                                    andTax:(NSDecimalNumber const *)inTax andShipping:(NSDecimalNumber const *)inShipping andErrorCode:(PayPalAmountErrorCode *)outErrorCode {
    //do any logic here that would adjust the amount based on the shipping address
    PayPalAmounts *newAmounts = [[PayPalAmounts alloc] init];
//    newAmounts.currency = @"USD";
//    newAmounts.payment_amount = (NSDecimalNumber *)inAmount;
//    
//    //change tax based on the address
//    if ([inAddress.state isEqualToString:@"CA"]) {
//        newAmounts.tax = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",[inAmount floatValue] * .1]];
//    } else {
//        newAmounts.tax = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",[inAmount floatValue] * .08]];
//    }
//    newAmounts.shipping = (NSDecimalNumber *)inShipping;
    
    //if you need to notify the library of an error condition, do one of the following
    //*outErrorCode = AMOUNT_ERROR_SERVER;
    //*outErrorCode = AMOUNT_CANCEL_TXN;
    //*outErrorCode = AMOUNT_ERROR_OTHER;
    
    return newAmounts;
}

//adjustAmountsAdvancedForAddress:andCurrency:andReceiverAmounts:andErrorCode: is optional. you only need to
//provide this method if you wish to recompute tax or shipping when the user changes his/her shipping address.
//for this method to be called, you must enable shipping and dynamic amount calculation on the PayPal object.
//the library will try to use this version first, but will use the simple one if this one is not implemented.
- (NSMutableArray *)adjustAmountsAdvancedForAddress:(PayPalAddress const *)inAddress andCurrency:(NSString const *)inCurrency
                                 andReceiverAmounts:(NSMutableArray *)receiverAmounts andErrorCode:(PayPalAmountErrorCode *)outErrorCode {
    NSMutableArray *returnArray = [NSMutableArray arrayWithCapacity:[receiverAmounts count]];
   
//    for (PayPalReceiverAmounts *amounts in receiverAmounts) {
//        //leave the shipping the same, change the tax based on the state
//        if ([inAddress.state isEqualToString:@"CA"]) {
//            amounts.amounts.tax = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",[amounts.amounts.payment_amount floatValue] * .1]];
//        } else {
//            amounts.amounts.tax = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",[amounts.amounts.payment_amount floatValue] * .08]];
//        }
//        [returnArray addObject:amounts];
//    }
    
    //if you need to notify the library of an error condition, do one of the following
    //*outErrorCode = AMOUNT_ERROR_SERVER;
    //*outErrorCode = AMOUNT_CANCEL_TXN;
    //*outErrorCode = AMOUNT_ERROR_OTHER;
    
    return returnArray;
}

//paymentLibraryExit is a required method. this is called when the library is finished with the display
//and is returning control back to your app. you should now do any user interface updates such as
//displaying a success/failure/canceled message.
- (void)paymentLibraryExit {
    UIAlertView *alert = nil;
    switch (status) {
        case PAYMENTSTATUS_SUCCESS:
            NSLog(@"Payment by PayPal - Success");
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"PROCESSING_PAYMENT", nil)];
            break;
        case PAYMENTSTATUS_FAILED:
            alert = [[UIAlertView alloc] initWithTitle:@"Order failed"
                                               message:@"Your order failed. Click on Pay with PayPal to try again."
                                              delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            break;
        case PAYMENTSTATUS_CANCELED:
            alert = [[UIAlertView alloc] initWithTitle:@"Order canceled"
                                               message:@"You canceled your order. Click on Pay with PayPal to try again."
                                              delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            break;
    }
    [alert show];
}


@end
