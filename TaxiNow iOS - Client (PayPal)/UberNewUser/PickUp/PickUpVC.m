//
//  PickUpVC.m
//  UberNewUser
//
//  Created by Elluminati - macbook on 27/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "PickUpVC.h"
#import "SWRevealViewController.h"
#import "AFNHelper.h"
#import "AboutVC.h"
#import "ContactUsVC.h"
#import "ProviderDetailsVC.h"
#import "CarTypeCell.h"
#import "UIImageView+Download.h"
#import "CarTypeDataModal.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "DriverList.h"
#import "sbMapAnnotation.h"

@interface PickUpVC ()
{
    NSString *strForUserId,*strForUserToken,*strForLatitude,*strForLongitude,*strForRequestID,*strForDriverLatitude,*strForDriverLongitude,*strForTypeid,*strForCurLatitude,*strForCurLongitude,*strPaymentMethod,*strForSourceLat,*strForSourceLong,*strForDestLat,*strForDestLong,*strForEstTime,*strForEstDist,*strForETAtime;
    NSInteger typeID;
    NSMutableArray *arrForInformation,*arrForApplicationType,*arrForAddress,*driversArr;
    BOOL isCOD, isCard, isPaypal, isPromo;
}
@property (weak, nonatomic) IBOutlet UIView *fareEstimateView;
@property (weak, nonatomic) IBOutlet UITextField *sourceLocationTxt;
@property (weak, nonatomic) IBOutlet UITextField *destLocationTxt;
@property (weak, nonatomic) IBOutlet UILabel *estimatedFareLbl;
@property (weak, nonatomic) IBOutlet UIButton *etaBtn;
@property (strong, nonatomic) IBOutlet UIView *applyPromoView;
@property (strong, nonatomic) IBOutlet UITextField *applyPromoTxt;
@property (strong, nonatomic) IBOutlet UIButton *promoBtn;

@end

@implementation PickUpVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    strForTypeid=@"0";
    self.btnCancel.hidden=YES;
    arrForAddress=[[NSMutableArray alloc]init];
    self.tableView.hidden=YES;
    [self setTimerToCheckDriverStatus];

    [self customFont];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateLocationManagerr];
    arrForApplicationType=[[NSMutableArray alloc]init];
    self.navigationController.navigationBarHidden=NO;
    
    self.viewForMarker.center=CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2-40);
    [self getAllApplicationType];
    [super setNavBarTitle:TITLE_PICKUP];
    [self customSetup];
    [self checkForAppStatus]; 
    [self getPagesData];
    [self getAllPaymentOptions];
    
    [self performSelector:@selector(showMapCurrentLocatinn) withObject:nil afterDelay:1.0];
//    [self performSelector:@selector(getNearbyProvidersForLat:andLong:) withObject:nil afterDelay:2.5];
}
-(void)viewDidAppear:(BOOL)animated
{
    //[self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];

}
-(void)viewDidDisappear:(BOOL)animated
{
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButtonItem addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

#pragma mark-
#pragma mark-

-(void)customFont
{
    self.txtAddress.font=[UberStyleGuide fontRegular];
    self.btnCancel=[APPDELEGATE setBoldFontDiscriptor:self.btnCancel];
    self.btnPickMeUp=[APPDELEGATE setBoldFontDiscriptor:self.btnPickMeUp];
    self.btnSelService=[APPDELEGATE setBoldFontDiscriptor:self.btnSelService];
}

#pragma mark -
#pragma mark - Location Delegate


-(void)updateLocationManagerr
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    
    [locationManager startUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    strForCurLatitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    strForCurLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];

        }
    }
    
    
}

#pragma mark -
#pragma mark - Mapview Delegate

-(void)showMapCurrentLocatinn
{
    CLLocationCoordinate2D l;
    l.latitude=[strForCurLatitude doubleValue];
    l.longitude=[strForCurLongitude doubleValue];
    
    [self.map setRegion:MKCoordinateRegionMake(l, MKCoordinateSpanMake(.1, .1)) animated:YES];
    [self getNearbyProvidersForLat:strForCurLatitude andLong:strForCurLongitude];
}
-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    // NSLog(@"Lat = %f, Log = %f",mapView.centerCoordinate.latitude,mapView.centerCoordinate.longitude);
    strForLatitude=[NSString stringWithFormat:@"%f",mapView.centerCoordinate.latitude];
    strForLongitude=[NSString stringWithFormat:@"%f",mapView.centerCoordinate.longitude];
    
    [self getNearbyProvidersForLat:strForLatitude andLong:strForLongitude];
    
    NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForLatitude floatValue], [strForLongitude floatValue], [strForLatitude floatValue], [strForLongitude floatValue]];
    
    NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
    
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                         options: NSJSONReadingMutableContainers
                                                           error: nil];
    
    NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
    NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
    NSArray *getAddress = [getLegs valueForKey:@"end_address"];
    if (getAddress.count!=0)
    {
        self.txtAddress.text=[[getAddress objectAtIndex:0]objectAtIndex:0];

    }
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:SEGUE_ABOUT])
    {
        AboutVC *obj=[segue destinationViewController];
        obj.arrInformation=arrForInformation;
    }
    else if([segue.identifier isEqualToString:SEGUE_TO_ACCEPT])
    {
        ProviderDetailsVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strForWalkStatedLatitude=strForDriverLatitude;
        obj.strForWalkStatedLongitude=strForDriverLongitude;
    }
    else if([segue.identifier isEqualToString:@"contactus"])
    {
        ContactUsVC *obj=[segue destinationViewController];
        obj.dictContent=sender;
    }
}

-(void)goToSetting:(NSString *)str
{
    [self performSegueWithIdentifier:str sender:self];
    
}

#pragma mark -
#pragma mark - UIButton Action

- (IBAction)pickMeUpBtnPressed:(id)sender
{
    NSMutableArray *paymentTypes = [[NSMutableArray alloc]initWithCapacity:0];
 //   if (isCOD) {
   //     [paymentTypes addObject:@"Pay via Cash"];
    //}
    if (isCard) {
        [paymentTypes addObject:@"Pay via Card"];
    }
    if (isPaypal) {
        [paymentTypes addObject:@"Pay via PayPal"];
    }
    
    //COD
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Payment method" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    popup.tag = 1;
    // ObjC Fast Enumeration
    for (NSString *title in paymentTypes) {
        [popup addButtonWithTitle:title];
    }

    popup.cancelButtonIndex = [popup addButtonWithTitle:@"Cancel"];
    
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

//COD
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {

    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSString *title = [popup buttonTitleAtIndex:buttonIndex];
    
    //if ([title isEqual:@"Pay via Cash"]) {
      //  NSLog(@"Pay via Cash - 0");
        //[pref setObject:@"0" forKey:PREF_PAYMENT_METHOD];
        //[self createRequest];
    //}
    if ([title isEqual:@"Pay via Card"]) {
        NSLog(@"Pay via Card - 1");
        [pref setObject:@"1" forKey:PREF_PAYMENT_METHOD];
        [self createRequest];
    }
    else if ([title isEqual:@"Pay via PayPal"]) {
        NSLog(@"Pay via PayPal - 2");
        [pref setObject:@"2" forKey:PREF_PAYMENT_METHOD];
        [self createRequest];
    }
}

// COD
-(void) createRequest {
    if([CLLocationManager locationServicesEnabled])
    {
        if ([strForTypeid isEqualToString:@"0"]||strForTypeid==nil)
        {
            strForTypeid=@"1";
        }
        if(![strForTypeid isEqualToString:@"0"])
        {
            if(((strForLatitude==nil)&&(strForLongitude==nil))
               ||(([strForLongitude doubleValue]==0.00)&&([strForLatitude doubleValue]==0)))
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
            }
            else
            {
                if([[AppDelegate sharedAppDelegate]connected])
                {
                    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
                    
                    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                    strForUserId=[pref objectForKey:PREF_USER_ID];
                    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                    strPaymentMethod=[pref objectForKey:PREF_PAYMENT_METHOD];
                    
                    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                    [dictParam setValue:strForLatitude forKey:PARAM_LATITUDE];
                    [dictParam setValue:strForLongitude  forKey:PARAM_LONGITUDE];
                    //[dictParam setValue:@"22.3023117"  forKey:PARAM_LATITUDE];
                    //[dictParam setValue:@"70.7969645"  forKey:PARAM_LONGITUDE];
                    [dictParam setValue:@"1" forKey:PARAM_DISTANCE];
                    [dictParam setValue:strForUserId forKey:PARAM_ID];
                    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
                    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
                    [dictParam setValue:@"2" forKey:PARAM_PAYMENT_TYPE];
                    
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_CREATE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         if (response)
                         {
                             if([[response valueForKey:@"success"]boolValue])
                             {
                                 NSLog(@"CREATE REQ -> %@",response);
                                 if([[response valueForKey:@"success"]boolValue])
                                 {
                                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                     
                                     strForRequestID=[response valueForKey:@"request_id"];
                                     [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                                     [self setTimerToCheckDriverStatus];
                                     
                                     [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
                                     self.btnCancel.hidden=NO;
                                     [APPDELEGATE.window addSubview:self.btnCancel];
                                     [APPDELEGATE.window bringSubviewToFront:self.btnCancel];
                                 }
                             }
                             else
                             {}
                         }
                         
                         
                     }];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                }
            }
            
        }
        else
            [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Taxinow -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
    }
}

- (IBAction)cancelReqBtnPressed:(id)sender
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]hideLoadingView];
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strReqId forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         [self.btnCancel removeFromSuperview];
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                         
                     }
                     else
                     {}
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }

    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Taxinow -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
         alertLocation.tag=100;
         [alertLocation show];
         

    }
}

- (IBAction)myLocationPressed:(id)sender
{
    [self showMapCurrentLocatinn];
}

- (IBAction)selectServiceBtnPressed:(id)sender
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        float closeY=(iOSDeviceScreenSize.height-self.btnSelService.frame.size.height);
       
        float openY=closeY-(self.bottomView.frame.size.height-self.btnSelService.frame.size.height);
        if (self.bottomView.frame.origin.y==closeY)
        {
            [UIView animateWithDuration:0.5 animations:^{
                [self.view bringSubviewToFront:self.bottomView];
                self.bottomView.frame=CGRectMake(0, openY, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
                
            } completion:^(BOOL finished)
             {
             }];
        }
        else
        {
            [UIView animateWithDuration:0.5 animations:^{
                [self.view bringSubviewToFront:self.bottomView];
                self.bottomView.frame=CGRectMake(0, closeY, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
                
            } completion:^(BOOL finished)
             {
             }];
        }
    }
}

#pragma mark -
#pragma mark - Custom WS Methods

-(void)getAllApplicationType
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_APPLICATION_TYPE withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSMutableArray *arr=[[NSMutableArray alloc]init];
                     [arr addObjectsFromArray:[response valueForKey:@"types"]];
                     for(NSMutableDictionary *dict in arr)
                     {
                         CarTypeDataModal *obj=[[CarTypeDataModal alloc]init];
                         obj.id_=[dict valueForKey:@"id"];
                         obj.name=[dict valueForKey:@"name"];
                         obj.icon=[dict valueForKey:@"icon"];
                         obj.is_default=[dict valueForKey:@"is_default"];
                         obj.price_per_unit_time=[dict valueForKey:@"price_per_unit_time"];
                         obj.price_per_unit_distance=[dict valueForKey:@"price_per_unit_distance"];
                         obj.base_price=[dict valueForKey:@"base_price"];
                         if ([obj.id_ isEqual:@"1"]) {
                             obj.isSelected=YES;
                         }
                         else {
                             obj.isSelected=NO;
                         }
                         [arrForApplicationType addObject:obj];
                     }
                     [self.collectionView reloadData];
                 }                 
                 
                 else
                 {}
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }

}
-(void)setTimerToCheckDriverStatus
{
    if (timerForCheckReqStatus) {
        [timerForCheckReqStatus invalidate];
        timerForCheckReqStatus = nil;
    }
    
     timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForRequestStatus) userInfo:nil repeats:YES];
}
-(void)checkForAppStatus
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
    
    if(strReqId!=nil)
    {
        [self checkForRequestStatus];
    }
    else
    {
        [self checkRequestInProgress];
    }
}
-(void)checkForRequestStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref objectForKey:PREF_REQ_ID];

        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSLog(@"GET REQ--->%@",response);
                     NSString *strCheck=[response valueForKey:@"walker"];
                     
                     if(strCheck)
                     {
                         [self.btnCancel removeFromSuperview];
                         
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                         strForDriverLatitude=[dictWalker valueForKey:@"latitude"];
                         strForDriverLongitude=[dictWalker valueForKey:@"longitude"];
                         if ([[response valueForKey:@"is_walker_rated"]integerValue]==1)
                         {
                             [pref removeObjectForKey:PREF_REQ_ID];
                             return ;
                         }
                         
                         ProviderDetailsVC *vcFeed = nil;
                         for (int i=0; i<self.navigationController.viewControllers.count; i++)
                         {
                             UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                             if ([vc isKindOfClass:[ProviderDetailsVC class]])
                             {
                                 vcFeed = (ProviderDetailsVC *)vc;
                             }
                             
                         }
                         if (vcFeed==nil)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
                         }else
                         {
                             [self.navigationController popToViewController:vcFeed animated:NO];
                         }
                         
                         
                         
                         
                     }
                     if([[response valueForKey:@"confirmed_walker"] intValue]==0 && [[response valueForKey:@"status"] intValue]==0)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref removeObjectForKey:PREF_REQ_ID];
                         
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_WALKER", nil)];
                         self.btnCancel.hidden=YES;
                         [self showMapCurrentLocatinn];
                         [APPDELEGATE hideLoadingView];
                     }
                     
//                     if([[response valueForKey:@"confirmed_walker"] intValue]==0 && [[response valueForKey:@"status"] isEqualToString:@"1"]) {
//                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
//                         [pref removeObjectForKey:PREF_REQ_ID];
//                         
//                         [self checkRequestInProgress];
//                     }
                     
                 }
             }
             
             else
             {}
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
/*
-(void)checkDriverStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
       // [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if([[response valueForKey:@"success"]boolValue])
             {
                 NSLog(@"GET REQ--->%@",response);
                 NSString *strCheck=[response valueForKey:@"walker"];
                 
                 if(strCheck)
                 {
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus=nil;
                     [[AppDelegate sharedAppDelegate]hideLoadingView];

                     [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
                 }
                 [[AppDelegate sharedAppDelegate]hideLoadingView];

                 
             }
             else
             {}
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}*/
-(void)checkRequestInProgress
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_REQUEST_PROGRESS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref setObject:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                     [pref synchronize];
                     [self checkForRequestStatus];
                 }
                 else
                 {}
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
-(void)getPagesData
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];

    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@",FILE_PAGE,PARAM_ID,strForUserId];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"Respond to Request= %@",response);
             [APPDELEGATE hideLoadingView];

             if (response)
             {
                 arrPage=[response valueForKey:@"informations"];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     //   [APPDELEGATE showToastMessage:@"Requset Accepted"];
                 }
             }
             
         }];
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}
#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForApplicationType.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CarTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cartype" forIndexPath:indexPath];

    [cell setCellData:[arrForApplicationType objectAtIndex:indexPath.row]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    for(CarTypeDataModal *obj in arrForApplicationType) {
        obj.isSelected = NO;
    }
    CarTypeDataModal *obj=[arrForApplicationType objectAtIndex:indexPath.row];
    obj.isSelected = YES;
    strForTypeid=[NSString stringWithFormat:@"%@",obj.id_];
    typeID = indexPath.row;
    [self.collectionView reloadData];
}

#pragma mark
#pragma mark - UITextfield Delegate


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSString *strFullText=[NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if(self.txtAddress==textField)
    {
        /*
         if([string isEqualToString:@""])
         {
         isSearching=NO;
         self.tableForCountry.frame=tempCountryRect;
         
         [self.tableForCountry reloadData];
         }
         else
         {
         isSearching=YES;
         self.tableForCountry.hidden=NO;
         [arrForFilteredCountry removeAllObjects];
         for (NSMutableDictionary *dict in self.arrForCountry) {
         NSString *tempStr=[dict valueForKey:@"country_name"];
         NSComparisonResult result = [tempStr compare:strFullText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [strFullText length])];
         if (result == NSOrderedSame) {
         [arrForFilteredCountry addObject:dict];
         }
         }*/
        
      
          //[self getLocationFromString:strFullText];
        
        if(arrForAddress.count==1)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x,86+134, self.tableView.frame.size.width, 44);
        else if(arrForAddress.count==2)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, 86+78, self.tableView.frame.size.width, 88);
        else if(arrForAddress.count==3)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, 86+34, self.tableView.frame.size.width, 132);
        else if(arrForAddress.count==0)
            self.tableView.hidden=YES;
        
        [self.tableView reloadData];
        
    }
    
    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.text=@"";
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
      [self getLocationFromString:textField.text forTextField:textField];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.tableView.hidden=YES;
    
    // self.tableForCountry.frame=tempCountryRect;
    //  self.tblFilterArtist.frame=tempArtistRect;
    
    
    [textField resignFirstResponder];
    return YES;
}


-(void)getLocationFromString:(NSString *)str forTextField:(UITextField *)textField
{
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"results"];
            
             if ([arrAddress count] > 0)
                 
             {
                 textField.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                 
                 NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                 if ([textField isEqual:self.txtAddress]) {
                     strForCurLatitude=[dictLocation valueForKey:@"lat"];
                     strForCurLongitude=[dictLocation valueForKey:@"lng"];
                     [self showMapCurrentLocatinn];
                 }
                 else if ([textField isEqual:self.sourceLocationTxt]) {
                     strForSourceLat = [dictLocation valueForKey:@"lat"];
                     strForSourceLong = [dictLocation valueForKey:@"lng"];
                 }
                 else if ([textField isEqual:self.destLocationTxt]) {
                     strForDestLat = [dictLocation valueForKey:@"lat"];
                     strForDestLong = [dictLocation valueForKey:@"lng"];
                 }
             }
             
         }
         
     }];
}

// fare Calculation
- (IBAction)showFareEstimate:(id)sender {
    self.fareEstimateView.frame = CGRectMake(0, 64, 320, 504);
    [self.view addSubview:self.fareEstimateView];
}

- (IBAction)calculateEstimate:(id)sender {
    [self getDistanceAndTimeFromLat:strForSourceLat andLong:strForSourceLong toLat:strForDestLat andLong:strForDestLong];
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    strForUserToken = [pref objectForKey:PREF_USER_TOKEN];
    strForUserId = [pref objectForKey:PREF_USER_ID];

    NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:strForUserId forKey:PARAM_ID];
    [dictParam setValue:strForEstDist forKey:PARAM_DISTANCE];
    [dictParam setValue:strForEstTime forKey:PARAM_TIME];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_FARE_CALCULATOR withParamData:dictParam withBlock:^(id response, NSError *error) {
        if (response) {
//            NSLog(@"FARE CALCULATOR -> %@",response);
            self.estimatedFareLbl.text = [NSString stringWithFormat:@"$ %.2f",[[response objectForKey:@"estimated_fare"] floatValue]];
        }
    }];

}

- (IBAction)hideFareEstimate:(id)sender {
    [self.fareEstimateView removeFromSuperview];
}

-(void)getDistanceAndTimeFromLat:(NSString *)soucelat andLong:(NSString *)sourceLong toLat:(NSString *)destLat andLong:(NSString *)destLong {
    NSString *distUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f",[soucelat floatValue],[sourceLong floatValue],[destLat floatValue],[destLong floatValue]];
        
        NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:distUrl] encoding:NSUTF8StringEncoding error:nil];
        
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                             options: NSJSONReadingMutableContainers
                                                               error: nil];
        NSArray *getDetails = [JSON objectForKey:@"rows"];
        NSArray *getRows = [[getDetails objectAtIndex:0] valueForKey:@"elements"];
        strForEstTime = [[[getRows objectAtIndex:0] valueForKey:@"duration"] objectForKey:@"value"];
        strForEstDist = [[[getRows objectAtIndex:0] valueForKey:@"distance"] objectForKey:@"value"];
        strForETAtime = [[[getRows objectAtIndex:0] valueForKey:@"duration"] objectForKey:@"text"];
}

- (IBAction)showRateCard:(id)sender {
    CarTypeDataModal *obj = [[CarTypeDataModal alloc]init];
    obj = [arrForApplicationType objectAtIndex:typeID];
    
    NSLog(@"BASE PRICE -> %@",obj.base_price);
    NSLog(@"UNIT TIME PRICE -> %@",obj.price_per_unit_time);
    NSLog(@"UNIT DIST PRICE -> %@",obj.price_per_unit_distance);
}

-(void)getAllPaymentOptions {
    NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_PAYMENT_OPTIONS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error) {
        if (response) {
            if ([[response valueForKey:@"success"] boolValue] == true) {
                NSLog(@"PAYMENT/PROMO OPTIONS -> %@",response);
                isCOD = [[[response valueForKey:@"payment_options"] objectForKey:@"cod"] boolValue];
                isCard = [[[response valueForKey:@"payment_options"] objectForKey:@"stored_cards"] boolValue];
                isPaypal = [[[response valueForKey:@"payment_options"] objectForKey:@"paypal"] boolValue];
                isPromo = [[response valueForKey:@"promo_allow"] boolValue];
//                if (isPromo) {
//                    self.promoBtn.hidden = NO;
//                }else {
//                    self.promoBtn.hidden = YES;
//                }
            }
        }
    }];
}

-(void)getNearbyProvidersForLat:(NSString *)latitude andLong:(NSString *)longitude {
    
    if(((strForLatitude==nil)&&(strForLongitude==nil))
       ||(([strForLongitude doubleValue]==0.00)&&([strForLatitude doubleValue]==0)))
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
        return;
    }
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:strForUserId forKey:PARAM_ID];
    [dictParam setValue:latitude forKey:PARAM_LATITUDE]; //12.911859
    [dictParam setValue:longitude forKey:PARAM_LONGITUDE]; //77.637862
    [dictParam setValue:@"1" forKey:PARAM_DISTANCE];
    if ([strForTypeid isEqualToString:@"0"]||strForTypeid==nil) {
        strForTypeid=@"1";
    }
    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_GET_NEARBY_PROVIDERS withParamData:dictParam withBlock:^(id response, NSError *error) {
        if (error) {
            [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_WALKER", nil)];
        }
        else if (response) {
            
            if ([[response valueForKey:@"success"]boolValue] == true) {
                driversArr = [[NSMutableArray alloc]initWithCapacity:0];
                [driversArr addObjectsFromArray:[response valueForKey:@"walkers"]];
                
                for (int index=0; index<[driversArr count]; index++) {
                    DriverList *driverDetails = [[DriverList alloc]init];
                    driverDetails.latitude = [[driversArr valueForKey:@"latitude"] objectAtIndex:index];
                    driverDetails.longitude = [[driversArr valueForKey:@"longitude"] objectAtIndex:index];
                    driverDetails.type = [[driversArr valueForKey:@"type"] objectAtIndex:index];
                    driverDetails.distance = [[driversArr valueForKey:@"distance"] objectAtIndex:index];
                    driverDetails.index = index;
                    
                    if (index == 0) {
                        [self getDistanceAndTimeFromLat:strForLatitude andLong:strForLongitude toLat:driverDetails.latitude andLong:driverDetails.longitude];
                        [self.etaBtn setTitle:[NSString stringWithFormat:@"ETA %@",strForETAtime] forState:UIControlStateNormal];
                    }
                    
                    //code to be executed in the background
                    [self addAnnotationWithTitle:self.txtAddress.text withLat:driverDetails.latitude andLong:driverDetails.longitude rateFor:driverDetails.timeCost tagVal:driverDetails.index];
                    
                }
            }
            else {
                [self.etaBtn setTitle:@"No Drivers" forState:UIControlStateNormal];
                [self.map removeAnnotations:self.map.annotations];
            }
        }
    }];
}

// Adding dummy Annotation (AC)
-(void) addAnnotationWithTitle:(NSString *)title withLat:(NSString *)latitude andLong:(NSString *)longitude rateFor:(NSString *)rate tagVal:(int)tag {
    CLLocationCoordinate2D coord;
    
    coord.latitude=[latitude floatValue];
    coord.longitude=[longitude floatValue];
    
    SBMapAnnotation *sbMapAnnot = [[SBMapAnnotation alloc]initWithCoordinate:coord];
    sbMapAnnot.title = [self getAddressForLat:latitude andLong:longitude];
    
    sbMapAnnot.yTag = tag;
    
    if (self.map.annotations.count == 0) {
        [self.map addAnnotation:sbMapAnnot];
    }
    
    for (SBMapAnnotation *existingPin in self.map.annotations)
    {
        if (![existingPin.title isEqualToString:sbMapAnnot.title]) {
            [self.map addAnnotation:sbMapAnnot];
        }
    }
}

-(NSString *) getAddressForLat:(NSString *)latitude andLong:(NSString *)longitude {
    
    NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[latitude floatValue], [longitude floatValue], [latitude floatValue], [longitude floatValue]];
    
    NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
    
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                         options: NSJSONReadingMutableContainers
                                                           error: nil];
    
    NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
    NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
    NSArray *getAddress = [getLegs valueForKey:@"end_address"];
    NSString *currentAdd;
    if (getAddress.count!=0)
    {
        currentAdd=[[getAddress objectAtIndex:0]objectAtIndex:0];
    }
    return currentAdd;
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    MKPinAnnotationView *pinAnnotation = nil;
    if(annotation != self.map.userLocation)
    {
        static NSString *defaultPinID = @"myDummyPin";
        pinAnnotation = (MKPinAnnotationView *)[self.map dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinAnnotation == nil )
            pinAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        SBMapAnnotation *sbanno = (SBMapAnnotation *)annotation;
        pinAnnotation.tag = sbanno.yTag;
        
        // Add a detail disclosure button to the callout.
        UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeSystem];
        UIImage *btnImg = [UIImage imageNamed:@"frwd_btn"];
        [detailButton setImage:btnImg forState:UIControlStateNormal];
        [detailButton setTintColor:[UIColor darkGrayColor]];
        CGFloat height = (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1) ? 55.f : 15.f;
        
        detailButton.frame = CGRectMake(0.f, 0.f, 32.f, height);
        
        // Add an image to the left callout.
        UIImageView *iconView = [[UIImageView alloc]init];
        
        iconView.frame = CGRectMake(2.0f, 0.0f, 32.0f, height);
        iconView.contentMode = UIViewContentModeScaleAspectFit;
        
        pinAnnotation.leftCalloutAccessoryView = iconView;
        
        pinAnnotation.calloutOffset = CGPointMake(0, 0);
        //        pinAnnotation.draggable=YES;
        
        if (pinAnnotation.tag == 1000) {
            pinAnnotation.image = [UIImage imageNamed:@"pin_client_org"];
            iconView.image = [UIImage imageNamed:@"pin_client_org"];
            pinAnnotation.canShowCallout=YES;
            pinAnnotation.rightCalloutAccessoryView = nil;
            pinAnnotation.leftCalloutAccessoryView = nil;
        }
        else if (pinAnnotation.tag == 1003) {
            pinAnnotation.image = [UIImage imageNamed:@"pin_drop"];
            iconView.image = [UIImage imageNamed:@"pin_drop"];
            pinAnnotation.canShowCallout=NO;
            pinAnnotation.rightCalloutAccessoryView = nil;
        }
        else {
            pinAnnotation.image = [UIImage imageNamed:@"pin_driver"];
            iconView.image = [UIImage imageNamed:@"pin_driver"];
            pinAnnotation.canShowCallout=NO;
            pinAnnotation.rightCalloutAccessoryView = nil;
        }
    }
    
    return pinAnnotation;
}
- (IBAction)showPromoView:(id)sender {
    self.applyPromoView.frame = CGRectMake(0, 64, 320, 504);
    [self.view addSubview:self.applyPromoView];
}

- (IBAction)applyPromoEvent:(id)sender {
    if ([self.applyPromoTxt.text isEqual:@""]) {
        [APPDELEGATE showToastMessage:@"Add a Promo code to avail offers"];
    }
    else {
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_CHECK_PROMO,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_PROMO,self.applyPromoTxt.text];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error) {
            if (response) {
                if ([[response valueForKey:@"success"]boolValue] == true) {
                    NSLog(@"CHECK PROMO -> %@",response);
                    [APPDELEGATE showToastMessage:@"Promo Added Successfully"];
                    [self.applyPromoView removeFromSuperview];
                }
                else {
                    [APPDELEGATE showToastMessage:@"Invalid Promo Code"];
                }
            }
            else {
                [APPDELEGATE showToastMessage:@"OOPS!! Something went wrong. Try Again"];
            }
        }];
    }
}

- (IBAction)cancelApplyPromoEvent:(id)sender {
    [self.applyPromoView removeFromSuperview];
}

@end

